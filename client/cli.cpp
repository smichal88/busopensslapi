/*
  * =====================================================================================
  *
  *       Filename:  cli.cpp
  *
  *    Description:  Simply client ssl for linux using openssl
  *
  *        Version:  1.0
  *        Created:  22.12.2011 02:03:32
  *       Revision:  none
  *       Compiler:  gcc
  *
  *         Author:  Michał Sobczak (smichal), smichal88@gmail.com
  *        Company:
  *
  * =====================================================================================
  */

#include <stdio.h>
#include <memory.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/ssl2.h>
#include <openssl/err.h>

//Define cer and key dir and file name
#define HOME "./cer/"
#define CERTF HOME "client.cer"
#define KEYF HOME  "client.key"
#define CACERTF HOME "ca.cer"

#define CHK_NULL(x) if((x) == NULL) exit(1)
#define CHK_ERR(err, s) if((err) == -1) {perror(s); exit(1); }
#define CHK_SSL(err) if((err) == -1) {ERR_print_errors_fp(stderr); exit(2); }

int main()
{
    int err = 0;
    SSL_CTX* ctx;
    SSL*     ssl;
    X509*    server_cert;
    char*    str;
    char buf[4096];
    SSL_METHOD* meth;

    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    meth = const_cast<SSL_METHOD*>(SSLv23_client_method());

    ctx = SSL_CTX_new(meth);                        CHK_NULL(ctx);

    if(SSL_CTX_use_certificate_file(ctx, CERTF, SSL_FILETYPE_PEM) <= 0)
    {
	ERR_print_errors_fp(stderr);
	exit(3);
    }

    if(SSL_CTX_use_PrivateKey_file(ctx, KEYF, SSL_FILETYPE_PEM) <= 0)
    {
	ERR_print_errors_fp(stderr);
	exit(4);
    }

    if(!SSL_CTX_check_private_key(ctx))
    {
	fprintf(stderr, "Private key does not match the certificate public key\n");
	exit(5);
    }

	//Read CA.
    if(!SSL_CTX_load_verify_locations(ctx, CACERTF, NULL))
    {
	fprintf(stderr, "CA read fail");
	exit(1);
    }

	//Set verify level
    SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, NULL);

    CHK_SSL(err);

	//Create connection
    BIO* conn;
    char server[] = "127.0.0.1:1111";
    conn = BIO_new_connect(server); CHK_NULL(conn);

    if(BIO_do_connect(conn) <= 0)
    {
	exit(8);
    }
	//Start SSL negotiation. */

    ssl = SSL_new(ctx);                         CHK_NULL(ssl);
    SSL_set_bio(ssl, conn, conn);
    err = SSL_connect(ssl);                     CHK_SSL(err);

    printf("SSL connection using %s\n", SSL_get_cipher(ssl));

	//Get server's certificate.

    server_cert = SSL_get_peer_certificate(ssl);       CHK_NULL(server_cert);
    printf("Server certificate:\n");

    str = X509_NAME_oneline(X509_get_subject_name(server_cert), 0, 0);
    CHK_NULL(str);
    printf("\t subject: %s\n", str);
    OPENSSL_free(str);

    str = X509_NAME_oneline(X509_get_issuer_name(server_cert), 0, 0);
    CHK_NULL(str);
    printf("\t issuer: %s\n", str);
    OPENSSL_free(str);

    X509_free(server_cert);

	//Send a message and receive a reply.

    err = SSL_write(ssl, "Hello World!", strlen("Hello World!"));  CHK_SSL(err);

    err = SSL_read(ssl, buf, sizeof(buf) - 1);                     CHK_SSL(err);
    buf[err] = '\0';
    printf("Got %d chars:'%s'\n", err, buf);

    SSL_shutdown(ssl);

    SSL_free(ssl);
    SSL_CTX_free(ctx);
} //main

